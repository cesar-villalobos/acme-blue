@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Search') }}</div>

                <div class="card-body">
                    <form method="GET" action="{{ route('search.search') }}">

                        <div class="form-group row">

                            <div class="col-md-10">
                                <input id="q" type="text" class="form-control" name="q" value="{{ old('q') }}" required autofocus>
                            </div>
                             <div class="col-md-2">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Search') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
