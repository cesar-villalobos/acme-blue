@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Search') }}</div>

                <div class="card-body">
                    <form method="GET" action="{{ route('search.search') }}">

                        <div class="form-group row">

                            <div class="col-md-10">
                                <input id="q" type="text" class="form-control" name="q" value="{{ $search }}" required autofocus>
                            </div>
                             <div class="col-md-2">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Search') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="card">
                <div class="card-header">{{ __('Results') }}</div>

                <div class="card-body">
                  <table class="table">
                    <tr>
                      <th class="w-25">Title</th>
                      <th class="w-75">Description</th>
                    </tr>
                    @isset($result)

                        @forelse($result as $resultItem)
                        <tr>
                          <td><a href="{{ route('media.show', $resultItem['id']) }}">{{$resultItem['title']}}</a></td>
                          <td>{{$resultItem['description']}}</td>
                        </tr>
                        @empty
                          <tr><td colspan="2">We couldn't found items that matches your search</td></tr>
                        @endforelse
                    @else
                      <tr><td  colspan="2">We couldn't found items that matches your search</td></tr>
                    @endisset


                  </table>
                </div>
            </div>


        </div>
    </div>
</div>
@endsection
