@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">History</div>

                <div class="card-body">
                  <table class="table">
                    <tr>
                      <th class="w-25">When</th>
                      <th class="w-75">Text Searched</th>
                    </tr>
                    @isset($history)

                        @forelse($history as $historyItem)
                        <tr>
                          <td>{{$historyItem['created_at']}}</td>
                          <td>{{$historyItem['searched']}}</td>
                        </tr>
                        @empty
                          <tr><td>You haven't searched any yet!</td></tr>
                        @endforelse
                    @else
                      <tr><td>You haven't searched any yet!</td></tr>
                    @endisset


                  </table>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
