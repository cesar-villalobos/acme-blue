@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            <div class="card">
                <div class="card-header">{{ __('Results') }}</div>

                <div class="card-body">

                  <table class="table">
                    @auth
                    <tr>
                      <th class="w-25">ID</th>
                      <td class="w-75">{{ $media->id}}</td>
                    </tr>
                    @endauth

                    <tr>
                      <th class="w-25">Filename</th>
                      <td class="w-75">{{ $media->filename}}</td>
                    </tr>
                    <tr>
                      <th class="w-25">Content-Type</th>
                      <td class="w-75">{{ $media->contenttype}}</td>
                    </tr>
                    <tr>
                      <th class="w-25">Title</th>
                      <td class="w-75">{{ $media->title}}</td>
                    </tr>
                    <tr>
                      <th class="w-25">Description</th>
                      <td class="w-75">{{ $media->description}}</td>
                    </tr>
                    <tr>
                      <th class="w-25">Created At</th>
                      <td class="w-75">{{ $media->created_at}}</td>
                    </tr>
                    @auth
                    <tr>
                      <th class="w-25">Share</th>
                      <td class="w-75" id="short"><a href="#" class="get-short" data-id="{{ $media->id}}" data-url="{{ route('index') . '/' }}">Get Tiny URL</a></td>
                    </tr>
                    <tr>
                      <th class="w-25">Like</th>
                      <td class="w-75" id="fav"><a href="#" class="add-fav" data-id="{{ $media->id}}" data-url="{{ route('index') . '/' }}">ADD to Fav</a></td>
                    </tr>
                    @endauth

                  </table>
                </div>
            </div>


        </div>
    </div>
</div>



@endsection
