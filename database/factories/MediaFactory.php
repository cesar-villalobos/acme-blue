<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Media;
use Faker\Generator as Faker;

$factory->define(Media::class, function (Faker $faker) {
    return [
        'filename' => Str::random(10) .".gif",
        'contenttype' => "image/gif",
        'title' => $faker->name,
        'description' => $faker->text,
    ];
});
