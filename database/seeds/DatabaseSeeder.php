<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('media')->insert([
            'filename' => Str::random(10).".gif",
            'contenttype' => 'image/gif',
            'title' => 'pet',
            'title' => 'description'
        ]);
    }
}
