<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fav extends Model
{
    //protected $table = "fav";
    protected $fillable = [
        'media_id', 'user_id'
    ];
}
