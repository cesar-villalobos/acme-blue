<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Log;
use Auth;
use App\Http\Resources\LogResource;

class LogController extends Controller
{
    /**
     * Display a listing of the resource ready for vue dattatable.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return LogResource::collection(Log::where("user_id", "=", Auth::user()->id)->orderBy("created_at", "DESC")->get());
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function history()
    {
        $log = Log::where("user_id", "=", Auth::user()->id)->orderBy("created_at", "DESC")->get();

        return view('history', ["history" => $log]);
    }

}
