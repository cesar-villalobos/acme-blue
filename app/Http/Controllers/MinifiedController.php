<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Minified;
use DB;

class MinifiedController extends Controller
{

    protected static $chars = "abcdfghjkmnpqrstvwxyz|ABCDFGHJKLMNPQRSTVWXYZ|0123456789";
    protected static $table = "minifieds";
    protected static $checkUrlExists = false;
    protected static $codeLength = 7;

    protected $timestamp;

    public function __construct(){
        $this->timestamp = date("Y-m-d H:i:s");
    }

    public function urlToShortCode($url){
        if(empty($url)){
            Log::error("No URL was supplied.");
            return false;
        }

      /*  if($this->validateUrlFormat($url) == false){
            Log::error("URL does not have a valid format.");
            return false;
        }

        if(self::$checkUrlExists){
            if (!$this->verifyUrlExists($url)){
                Log::error("URL does not appear to exist.");
                return false;
            }
        }*/

        $shortCode = $this->urlExistsInDB($url);
        if($shortCode == false){
            $shortCode = $this->createShortCode($url);
        }

        return $shortCode;
    }

    protected function validateUrlFormat($url){
        return filter_var($url, FILTER_VALIDATE_URL, FILTER_FLAG_HOST_REQUIRED);
    }

    protected function verifyUrlExists($url){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_setopt($ch,  CURLOPT_RETURNTRANSFER, true);
        curl_exec($ch);
        $response = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return (!empty($response) && $response != 404);
    }

    protected function urlExistsInDB($url){

        $result = DB::table(self::$table)->where('long_url', $url)->first();
        return (empty($result)) ? false : $result->short_code;
    }

    protected function createShortCode($url){
        $shortCode = $this->generateRandomString(self::$codeLength);
        $id = $this->insertUrlInDB($url, $shortCode);
        return $shortCode;
    }

    protected function generateRandomString($length = 6){
        $sets = explode('|', self::$chars);
        $all = '';
        $randString = '';
        foreach($sets as $set){
            $randString .= $set[array_rand(str_split($set))];
            $all .= $set;
        }
        $all = str_split($all);
        for($i = 0; $i < $length - count($sets); $i++){
            $randString .= $all[array_rand($all)];
        }
        $randString = str_shuffle($randString);
        return $randString;
    }

    protected function insertUrlInDB($url, $code){
        return DB::table(self::$table)->insertGetId(
            ['long_url' => $url, 'short_code' => $code, 'created_at' => $this->timestamp ]
        );
    }

    public function shortCodeToUrl($code){
        if(empty($code)) {
                Log::error("No short code was supplied.");
                return false;
        }

        if($this->validateShortCode($code) == false){
                Log::error("Short code does not have a valid format.");
                return false;
        }

        $urlRow = $this->getUrlFromDB($code);
        if(empty($urlRow)){
            Log::error("Short code does not appear to exist.");
            return false;
        }

        return $urlRow->long_url;
    }

    protected function validateShortCode($code){
        $rawChars = str_replace('|', '', self::$chars);
        return preg_match("|[".$rawChars."]+|", $code);
    }

    protected function getUrlFromDB($code){

        $result = DB::table(self::$table)->where('short_code', $code)->first();
        return (empty($result)) ? false : $result;
    }

    public function index()
    {
        //return Minified::get();
        return view('test');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return ["short_url" => $this->urlToShortCode(request('media'))];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return  Minified::find($id);
    }

    /**
     * Redirect to media Url.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirect($short)
    {
        return redirect(route('media.index')."/". $this->shortCodeToUrl($short));
    }

}
