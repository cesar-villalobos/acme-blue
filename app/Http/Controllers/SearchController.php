<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Media;
use App\Log;
use Auth;

class SearchController extends Controller
{

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $q = (empty(request('q'))) ? "" : request('q');
    	Log::create([
            'searched' => $q,
            'user_id' => Auth::user()->id
        ]);

        return Media::where('title', 'like', '%' . $q  . '%')->orWhere('description', 'like', '%' . $q  . '%')->get();
    }

    public function search()
    {
        $q = (empty(request('q'))) ? "" : request('q');
        Log::create([
            'searched' => $q,
            'user_id' => Auth::user()->id
        ]);

        $result = Media::where('title', 'like', '%' . $q  . '%')->orWhere('description', 'like', '%' . $q  . '%')->get();


        return view('search', ["result" =>$result, "search" => $q]);
    }
}
