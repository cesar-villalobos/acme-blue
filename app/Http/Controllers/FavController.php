<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Fav;
use Auth;

class FavController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return Fav::create([
            'media_id' => request('media'),
            'user_id' => Auth::user()->id
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Fav::find($id);
    }
}
