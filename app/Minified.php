<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Minified extends Model
{
    //protected $table = "minified";
    protected $fillable = [
        'short_code', 'long_url'
    ];
}
