<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('home');
})->name('index');

Auth::routes();

Route::get('/test', 'MinifiedController@index')->name('test');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/history', 'LogController@history')->name('log.history')->middleware('auth');
Route::get('/search', 'SearchController@search')->name('search.search')->middleware('auth');

Route::get('/log', 'LogController@index')->name('log.index')->middleware('auth');
Route::get('/api-search', 'SearchController@index')->name('search.index')->middleware('auth');


Route::get('/media', 'MediaController@index')->name('media.index')->middleware('auth');
Route::get('/media/{id}', 'MediaController@show')->name('media.show');

Route::post('/fav/create', 'FavController@store')->name('fav.store')->middleware('auth');
Route::get('/fav/{id}', 'FavController@show')->name('fav.show')->middleware('auth');

Route::post('/minified/create', 'MinifiedController@store')->name('minified.store')->middleware('auth');
Route::get('/minified/{id}', 'MinifiedController@show')->name('minified.show')->middleware('auth');

Route::get('/{short}', 'MinifiedController@redirect')->name('minified.redirect');





