Steps to install 

1.- clone repository

2.- configure your .env file

3.- inside project folder run "composer install"

4.- run "php artisan migrate:fresh"

5.- Optional you can run data for media (gif) to search

----
INSERT INTO `media` (`id`, `filename`, `contenttype`, `title`, `description`, `created_at`, `updated_at`) VALUES (1, 'dog.gif', 'image/gif', 'a dog\'s life animals', 'a beautiful image with dogs in new families', '2019-08-05 00:01:15', '2019-08-05 00:01:16');
INSERT INTO `media` (`id`, `filename`, `contenttype`, `title`, `description`, `created_at`, `updated_at`) VALUES (2, 'pet.gif', 'image/gif', 'pet stories', 'a lot of new animal friends', '2019-08-05 00:01:55', '2019-08-05 00:01:55');
INSERT INTO `media` (`id`, `filename`, `contenttype`, `title`, `description`, `created_at`, `updated_at`) VALUES (3, 'light.gif', 'image/gif', 'new ideas', 'lorem ipsum dolor sit amet', '2019-08-05 00:02:20', '2019-08-05 00:02:21');
INSERT INTO `media` (`id`, `filename`, `contenttype`, `title`, `description`, `created_at`, `updated_at`) VALUES (4, 'animal.gif', 'image/gif', 'animal life', 'dog, cats and so much more', NULL, NULL);
-----

6.- execute app and register an user
7.- log in with new user,  you can search and each text will be logged and you can see in history.  On detail page you can add to fav and generate short url.


Short URL class first check in db if there is a large url stored.  If is yes return short url generated before.

If is a new URL, generate a random string (could be setted by parameters, or generate also in a random lenght, currently lengh is fixed to 7 chars).
The new short string is verified in DB in case of duplicated, if is not duplicated a new record is inserted in db for new short url.

The proces to generate a random string is so simple, first you have a string of possible characters, and for each iteration randomly you get a char and concatenate with desired lengh of string.


Demo is installed in http://104.248.76.224


